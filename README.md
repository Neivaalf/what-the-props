# what-the-props

This project have multiple goals:

- Play with Babel
- Play with Commander + React Ink
- Play with GraphQL
- Play with Lerna
- Play with npm publishing
- Support my company design system initiative by gathering data about the usage of its components/props

It is made of three parts:

## Scrapper

Example: `npm start -- -d -t PrimaryCard SecondaryCard h1 Link Loading Filter -p ../client/src `

This package will crawl all pages and components from a specific folder, looking for components that will match its allowlist. Once found, it will store in a SQLite DB, the component, the props and their values. It relies on Babel to parse the code into AST, and go through it.

You can run this part periodically, or on demand.

## Server

Example: `npm run start -- ../scrapper/database.db`

This package is simply exposing a GraphQL schema of the Scrapper-generated data.

You can use it directly in your Storybook if you want to have real-life usage data of your components directly in your documentation.

## Client

You can find how many different components were found, and the one that have the most occurences. You can browse the full list easily.

![alt text](image-3.png)

Clicking on a component will give you more details, such as the number of different props found, and the one that have the most occurences. You can browse the full list easily.

This is a good place to reflect on your component API and responsibility.

![alt text](image-4.png)

Clicking on a prop will give you more details, such as the number of different values found, and the one that have the most occurences. You can browse the full list easily.

It's a good place to reflect on your component API again.

In our example, the button appears 350 times. 65% of the time (230), we're using the `color` prop. When we're using the `color` prop, it's 80% of the time (190) for the value `primary`. It means that 54% (190/350) of our buttons are primaries.

We can also notice low occurences of the `default` value, or `{buttonColor}`.

![alt text](image-5.png)

Actually, you'd like to refactor to remove the `default` value to simplify your component and improve consistency. Since we also scrapped the places we found those occurences, clicking on the filepath will open VSCode at the exact right spot.

![alt text](image-6.png)

Using this method, you can also deprecate your components with a simple renaming. From there you can easily track the usage of deprecated components, and have an exact list of the places where you need to refactor the leftovers, making it easier to estimate the required effort.

![alt text](image-7.png)

## Storybook Bonus

![alt text](image.png)

![alt text](image-1.png)

![alt text](image-2.png)
